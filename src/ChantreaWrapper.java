
import java.util.Arrays;
public class ChantreaWrapper<E> {
    private static int DEFAULT_CAPACITY=10;
    private static int count=0;
    private Object[] elementData=new Object[DEFAULT_CAPACITY];
    boolean isTrue=false;
    public void addItem(E e) throws DuplicateValueException, NullValueException {
        if(DEFAULT_CAPACITY==count){
            DEFAULT_CAPACITY=DEFAULT_CAPACITY+10;
            elementData= Arrays.copyOf(elementData,DEFAULT_CAPACITY);
        }
        if(e==null){
            throw new NullValueException("Inputed null value");
        }
        for(Object obj : elementData){
            if(obj==e){
                throw new DuplicateValueException("Duplicate Value: "+obj);
            }else{
                isTrue=true;
            }
        }
        if(isTrue==true){
            elementData[count]=e;
            count++;
        }
    }
    public Object getItem(int index){
        return elementData[index];
    }
    public int size() {
        return count;
    }
}
