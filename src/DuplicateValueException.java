public class DuplicateValueException extends Exception {
    public DuplicateValueException(){
        super();
    }
    public DuplicateValueException(String message){
        super(message);
    }
}
